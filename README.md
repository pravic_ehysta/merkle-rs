# BitFury test task

This library implements a [Merkle Tree](https://en.wikipedia.org/wiki/Merkle_tree) data structure with additional useful routines.


## Description

Library contains the following modules:

* *hash* - provides a SHA-256 hashing functions and concatenation
* *blocks* - functions and iterators to generate a 1024 byte hashed blocks from the source input
* *fold* - blocks folding functions to compute Merkle hash root without building tree
* *tree* - Merkle tree itself


## Features

Current Merkle tree implementation builds the tree from the source blocks. 
It allows to access the root of computed blocks (Merkle hash root) and sequence of blocks for any tree level (depth).

In general, Merkle trees used for the following purposes:

* Big data validation by splitting it to amount of fixed size blocks and calculating hash of them. That blocks can be verified later against its hashes.
* Invalid blocks detection (not clear understanding yet).
* Single block verification without complete tree presence: block verified only against its direct parent nodes.

This library implements only first feature.


## Implementation

This library uses a SHA-256 hash from [rust-crypto](https://crates.io/crates/rust-crypto), but it can be replaced with any stable and well-known cryptographic library.


## Status

Very unstable and not ready for production use. It is better to redesign it based on complete usage requirements and cases.

Tested under Rust stable and nightly.

