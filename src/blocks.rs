//! Blocks generation.

#[doc(hidden)]
pub use super::{Block, BLOCK_SIZE};

use hash::dhash;


/// A hashing iterator over input source splitted by blocks.
#[derive(Clone)]
pub struct Blocks<'a> {
  chunks: ::std::slice::Chunks<'a, u8>,
}

/// `Iterator` trait implementation.
impl<'a> Iterator for Blocks<'a> {
  type Item = Block;

  /// Returns the next hashed block.
  fn next(&mut self) -> Option<Block> {
    self.chunks.next().map(dhash)
  }

  fn size_hint(&self) -> (usize, Option<usize>) {
    self.chunks.size_hint()
  }

  fn count(self) -> usize {
    self.chunks.count()
  }
}

/// Return a hashing iterator over source blocks.
///
/// It yields a double hashed 1024 byte blocks.
pub fn blocks(source: &[u8]) -> Blocks {
  Blocks { chunks: source.chunks(BLOCK_SIZE) }
}

/// Split the `source` with the double hashed 1024 byte blocks.
pub fn make_blocks(source: &[u8]) -> Vec<Block> {
  let mut out = Vec::with_capacity((source.len() + BLOCK_SIZE - 1) / BLOCK_SIZE);
  for chunk in source.chunks(BLOCK_SIZE) {
    out.push(dhash(chunk));
  }
  out
}


#[cfg(test)]
mod tests {
  use super::{Block, make_blocks, blocks};

  use std::iter::repeat;

  #[test]
  fn make_blocks_works() {
    let n = 0;
    let v: Vec<u8> = repeat(0).take(n).collect();
    assert_eq!(make_blocks(&v).len(), 0);

    let n = 1;
    let v: Vec<u8> = repeat(0).take(n).collect();
    assert_eq!(make_blocks(&v).len(), 1);

    let n = 1023;
    let v: Vec<u8> = repeat(0).take(n).collect();
    assert_eq!(make_blocks(&v).len(), 1);

    let n = 1024;
    let v: Vec<u8> = repeat(0).take(n).collect();
    assert_eq!(make_blocks(&v).len(), 1);

    let n = 2048;
    let v: Vec<u8> = repeat(0).take(n).collect();
    assert_eq!(make_blocks(&v).len(), 2);
  }

  #[test]
  fn blocks_works() {
    let n = 0;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 0);

    let n = 1;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 1);

    let n = 1023;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 1);

    let n = 1024;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 1);

    let n = 1025;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 2);

    let n = 2048;
    let v: Vec<u8> = repeat(0).take(n).collect();
    let b: Vec<Block> = blocks(&v).collect();
    assert_eq!(b.len(), 2);
  }
}
