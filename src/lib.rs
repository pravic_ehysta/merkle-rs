//! Merkle Tree implementation.

#![cfg_attr(feature = "with-bench", feature(test))]
#[cfg(all(test, feature = "with-bench"))]
extern crate test;

extern crate crypto;
extern crate rustc_serialize as serialize;

#[doc(hidden)]
pub type Block = [u8; 32];

#[doc(hidden)]
pub const BLOCK_SIZE: usize = 1024;

mod hash;
mod fold;
mod tree;

pub mod blocks;

#[doc(hidden)]
pub use blocks::{blocks, make_blocks};

pub use fold::fold;
pub use tree::MerkleTree;
