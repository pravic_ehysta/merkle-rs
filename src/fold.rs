//! Blocks folding.
pub use super::{Block, BLOCK_SIZE};
use hash::{dhash, dconcat};


/// Return even number for specified one.
pub fn even(n: usize) -> usize {
  n + n % 2
}


/// Fold specified blocks by hashing and concatenating it.
pub fn fold_two(a: &Block, b: &Block) -> Block {
  let d1 = dhash(a);
  let d2 = dhash(b);
  dconcat(&d1, &d2)
}

/// Fold specified slice with 1 or 2 blocks by hashing and concatenating it.
pub fn fold_slice(blocks: &[Block]) -> Block {
  let d1 = dhash(&blocks[0]);
  let d2 = dhash(blocks.last().unwrap());
  dconcat(&d1, &d2)
}

/// Fold two optional blocks.
#[cfg(test)]
pub fn fold_opt(a: Option<&Block>, b: Option<&Block>) -> Block {
  let da = a.unwrap();
  let d1 = dhash(da);
  let d2 = dhash(b.unwrap_or(da));
  dconcat(&d1, &d2)
}

fn fold_recursive(input: &[Block]) -> Block {
  if input.len() == 0 {
    panic!("fold_recursive called with empty input!");
  }
  if input.len() == 1 {
    return input[0];
  }

  let mut out = Vec::with_capacity(even(input.len()) / 2);
  for chunk in input.chunks(2) {
    out.push(fold_slice(chunk));
  }
  if out.len() > 1 {
    fold_recursive(&out)
  } else {
    out[0]
  }
}

#[cfg(test)]
fn fold_inplace(input: &[Block]) -> Block {
  if input.len() == 0 {
    panic!("fold_inplace called with empty input!");
  }
  if input.len() == 1 {
    return input[0];
  }

  let mut out = Vec::with_capacity(even(input.len()) / 2);

  for chunk in input.chunks(2) {
    out.push(fold_slice(chunk));
  }

  let mut len = out.len();
  while len > 1 {
    let (mut i, mut j) = (0, 0);
    while i < len {
      let h = fold_opt(out.get(i), out.get(i + 1));
      out[j] = h;
      i += 2;
      j += 1;
    }
    len = j;
  }
  out[0]
}

/// Fold sequence of the source blocks into tree hashing root.
pub fn fold(input: &[Block]) -> Block {
  fold_recursive(input)
}


#[cfg(test)]
mod tests {
  use super::{fold_recursive, fold_inplace};
  use serialize::hex::ToHex;

  #[test]
  fn fold_three_blocks_recursive() {
    let zero = [0u8; 32];
    let blocks = [zero; 3];
    let root = fold_recursive(&blocks);
    assert_eq!("e627511c1fbeab8ce4e81dec77fa723655340bd8a335990b426747eadb83cc4c",
               root.to_hex());
  }

  #[test]
  fn fold_three_blocks_inplace() {
    let zero = [0u8; 32];
    let blocks = [zero; 3];
    let root = fold_inplace(&blocks);
    assert_eq!("e627511c1fbeab8ce4e81dec77fa723655340bd8a335990b426747eadb83cc4c",
               root.to_hex());
  }
}


#[cfg(all(test, feature = "with-bench"))]
mod benchmarks {
  use blocks::make_blocks;
  use super::{fold_inplace, fold_recursive};
  use ::test;

  #[bench]
  fn bench_fold_inplace_1kb(b: &mut test::Bencher) {
    let cb = 1024 * 1;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_inplace(&blocks);
    })
  }

  #[bench]
  fn bench_fold_recursive_1kb(b: &mut test::Bencher) {
    let cb = 1024 * 1;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_recursive(&blocks);
    })
  }

  #[bench]
  fn bench_fold_inplace_1mb(b: &mut test::Bencher) {
    let cb = 1024 * 1024;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_inplace(&blocks);
    })
  }

  #[bench]
  fn bench_fold_recursive_1mb(b: &mut test::Bencher) {
    let cb = 1024 * 1024;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_recursive(&blocks);
    })
  }

  #[bench]
  fn bench_fold_inplace_128mb(b: &mut test::Bencher) {
    let cb = 1024 * 1024 * 16;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_inplace(&blocks);
    })
  }

  #[bench]
  fn bench_fold_recursive_128mb(b: &mut test::Bencher) {
    let cb = 1024 * 1024 * 16;
    let mut data = Vec::new();
    data.resize(cb, 0u8);
    let blocks = make_blocks(&data);

    b.iter(|| {
      fold_recursive(&blocks);
    })
  }
}
