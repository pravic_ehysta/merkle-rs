//! Merkle Tree implementation.

use super::Block;
use fold::{even, fold_two};


/// A tree constructed by hashing paired data (the leaves).
///
/// This merkle tree constructed from source block sequence and backed by
/// vector (in breadth-first order from bottom to top - `root`).
///
/// Five-block tree diagram:
///
/// ```text
///        ABCDEEEE .......Merkle root
///       /        \
///    ABCD        EEEE
///   /    \      /
///  AB    CD    EE .......E is paired with itself
/// /  \  /  \  /
/// A  B  C  D  E
/// ```
/// And its memory layout:
///
/// ```text
/// |A,B,C,D,E|AB,CD,EE|ABCD,EEEE|ABCDEEEE|
/// ```
///
#[derive(Default)]
pub struct MerkleTree {
  nodes: Vec<Block>,
  blocks_count: usize,
  level_count: usize,
}

impl MerkleTree {
  /// Create a new, empty tree.
  pub fn new() -> MerkleTree {
    MerkleTree {
      nodes: Vec::new(),
      blocks_count: 0,
      level_count: 0,
    }
  }

  /// Clear tree removing all nodes.
  pub fn clear(&mut self) {
    self.nodes.clear();
    self.blocks_count = 0;
    self.level_count = 0;
  }

  /// Width of the tree.
  pub fn breadth(&self) -> usize {
    self.blocks_count
  }

  /// Depth of the tree.
  pub fn depth(&self) -> usize {
    self.level_count
  }

  /// Returns `true` if tree contains no data.
  pub fn is_empty(&self) -> bool {
    self.blocks_count == 0
  }

  /// Returns the root node for non-empty tree.
  pub fn root(&self) -> Option<&Block> {
    self.nodes.last()
  }

  /// Append the source block to the base of tree.
  pub fn add_block(&mut self, block: &Block) {
    self.nodes.insert(self.blocks_count, *block);
    self.blocks_count += 1;
    if self.level_count > 0 {
      self.commit();
    }
  }

  /// Append the source block to the base of tree.
  pub fn put_block(&mut self, block: Block) {
    self.nodes.insert(self.blocks_count, block);
    self.blocks_count += 1;
    if self.level_count > 0 {
      self.commit();
    }
  }

  /// Build tree from the source blocks.
  pub fn commit(&mut self) {
    self.build_tree();
  }

  /// Return blocks for specified `level` of tree (`0` is root)
  /// up to `self.depth()` level.
  pub fn get_line(&self, level: usize) -> Option<&[Block]> {
    if self.is_empty() || level >= self.level_count {
      return None;
    }
    // |1|
    // |12|3|
    // |123|45|6|
    // |1234|56|7|
    // |12345|123|12|1|
    // |123456|123|12|1|
    // |1234567|1234|12|1|
    let level = self.level_count - level - 1;

    let mut src_pos = 0;
    let mut level_size = self.breadth();
    for _ in 0..level {
      src_pos += level_size;
      level_size = even(level_size) / 2;
    }
    let end_pos = src_pos + level_size;
    Some(&self.nodes[src_pos..end_pos])
  }


  fn build_tree(&mut self) {

    self.nodes.truncate(self.blocks_count);
    self.level_count = 1;

    let mut size = self.blocks_count;
    let mut nodes_count = size;
    while size > 1 {
      size = even(size) / 2;
      nodes_count += size;
      self.level_count += 1;
    }

    // TODO: level count: ceil(log2(2 * blocks_count))
    //

    if self.nodes.capacity() < nodes_count {
      let cap = self.nodes.capacity();
      self.nodes.reserve(nodes_count - cap);
    }

    self.build_inplace()
  }

  fn build_inplace(&mut self) {
    let mut level_size = self.blocks_count;
    if level_size <= 1 {
      self.level_count = level_size;	// 0||1
      return;
    }

    let mut out = &mut self.nodes;
    let mut src_pos = 0;
    let mut level = 0;
    while level_size > 1 {
      let end_pos = src_pos + level_size;
      while src_pos < end_pos {
        let a = src_pos;
        let b = ::std::cmp::min(src_pos + 1, end_pos - 1);
        let d = fold_two(&out[a], &out[b]);
        out.push(d);
        src_pos = b + 1;
      }
      level_size = even(level_size) / 2;
      level += 1;
    }
    level += 1;
    assert_eq!(level, self.level_count);
    assert_eq!(level_size, 1);
  }
}

/// Debug tree visualization.
impl ::std::fmt::Debug for MerkleTree {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
    try!(write!(f, "|"));
    for i in (0..self.depth()).rev() {
      let line = self.get_line(i).unwrap();
      try!(write!(f, "{:?}|", line));
    }
    Ok(())
  }
}


#[cfg(test)]
mod test {
  use ::Block;
  use super::MerkleTree;
  use std::iter::repeat;

  const ZERO_BLOCK: Block = [0u8; 32];

  #[test]
  fn empty_tree() {
    let m = MerkleTree::new();
    assert_eq!(m.is_empty(), true);
    assert_eq!(m.breadth(), 0);
    println!("tree 0 {:?}", m);
  }

  #[test]
  fn one_block() {
    let mut m = MerkleTree::new();
    for b in repeat(ZERO_BLOCK).take(1) {
      m.put_block(b.to_owned());
    }
    m.commit();
    println!("tree 1 {:?}", m);
    assert_eq!(m.is_empty(), false);
    assert_eq!(m.depth(), 1);
    assert_eq!(m.breadth(), 1);
    assert_eq!(m.root(), Some(&ZERO_BLOCK));
  }

  #[test]
  fn two_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..3 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 2 {:?}", m);
    assert_eq!(m.depth(), 2);
    assert_eq!(m.breadth(), 2);
  }

  #[test]
  fn three_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..4 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 3 {:?}", m);
    assert_eq!(m.depth(), 3);
    assert_eq!(m.breadth(), 3);
  }

  #[test]
  fn four_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..5 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 4 {:?}", m);
    assert_eq!(m.depth(), 3);
    assert_eq!(m.breadth(), 4);
  }

  #[test]
  fn five_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..6 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 5 {:?}", m);
    assert_eq!(m.depth(), 4);
    assert_eq!(m.breadth(), 5);
  }

  #[test]
  fn make_6_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..7 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 6 {:?}", m);
    assert_eq!(m.depth(), 4);
    assert_eq!(m.breadth(), 6);
  }

  #[test]
  fn make_10_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..11 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 10 {:?}", m);
    assert_eq!(m.depth(), 5);
    assert_eq!(m.breadth(), 10);
  }

  #[test]
  fn three_plus_two_blocks() {
    let mut m = MerkleTree::new();
    for _ in 1..4 {
      m.put_block(ZERO_BLOCK);
    }
    m.commit();
    println!("tree 3.0 {:?}", m);
    assert_eq!(m.depth(), 3);
    assert_eq!(m.breadth(), 3);

    for _ in 1..3 {
      m.put_block(ZERO_BLOCK);
    }

    println!("tree 3.2 {:?}", m);
    assert_eq!(m.depth(), 4);
    assert_eq!(m.breadth(), 5);
  }
}
