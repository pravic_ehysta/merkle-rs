//! Hash module.
use crypto::digest::Digest;
use crypto::sha2::Sha256;
use super::Block;


/// Double SHA-256 hash of input.
pub fn dhash(data: &[u8]) -> Block {
  let mut digest = [0; 32];
  let mut hash = Sha256::new();

  hash.input(data);
  hash.result(&mut digest);
  hash.reset();

  hash.input(&digest);
  hash.result(&mut digest);

  digest
}

/// Double hash of concatenated blocks.
pub fn dconcat(a: &Block, b: &Block) -> Block {
  let mut digest = [0; 32];
  let mut hash = Sha256::new();

  hash.input(a);
  hash.input(b);
  hash.result(&mut digest);
  hash.reset();

  hash.input(&digest);
  hash.result(&mut digest);

  digest
}


#[cfg(test)]
mod tests {
  use crypto::digest::Digest;
  use crypto::sha2::Sha256;
  use serialize::hex::ToHex;

  fn sha256str(data: &str) -> String {
    let mut hash = Sha256::new();
    hash.input_str(data);
    hash.result_str()
  }

  fn sha256_double(data: &[u8]) -> Vec<u8> {
    let mut hash = Sha256::new();

    let mut interium = [0; 32];

    // round 1
    hash.input(data);
    hash.result(&mut interium);
    hash.reset();

    // round 2
    hash.input(&interium);
    hash.result(&mut interium);

    interium.as_ref().to_owned()
  }

  fn sha256dstr(data: &str) -> String {
    let digest = sha256_double(data.as_bytes());
    digest[..].to_hex()
  }

  #[test]
  fn sha256_works() {
    assert_eq!("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
               sha256str(""));
    assert_eq!("5df6e0e2761359d30a8275058e299fcc0381534545f55cf43e41983f5d4c9456",
               sha256dstr(""));
    assert_eq!("2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824",
               sha256str("hello"));
    assert_eq!("9595c9df90075148eb06860365df33584b75bff782a510c6cd4883a419833d50",
               sha256dstr("hello"));
    assert_eq!("d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592",
               sha256str("The quick brown fox jumps over the lazy dog"));
    assert_eq!("6d37795021e544d82b41850edf7aabab9a0ebe274e54a519840c4666f35b3937",
               sha256dstr("The quick brown fox jumps over the lazy dog"));
  }
}
